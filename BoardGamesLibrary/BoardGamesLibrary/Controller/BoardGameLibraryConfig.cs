﻿namespace BoardGamesLibrary.Controller
{
    /// <summary>
    /// Configuration description
    /// </summary>
    public class BoardGameLibraryConfig
    {
        /// <summary>
        /// The use bar code reader
        /// </summary>
        private bool useBarCodeReader = false;

        /// <summary>
        /// The bar code COM port
        /// </summary>
        private string barCodeComPort = "COM8";

        /// <summary>
        /// Gets or sets a value indicating whether [use bar code reader].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use bar code reader]; otherwise, <c>false</c>.
        /// </value>
        public bool UseBarCodeReader { get => useBarCodeReader; set => useBarCodeReader = value; }

        /// <summary>
        /// Gets or sets the bar code COM port.
        /// </summary>
        /// <value>
        /// The bar code COM port.
        /// </value>
        public string BarCodeComPort { get => barCodeComPort; set => barCodeComPort = value; }
    }
}