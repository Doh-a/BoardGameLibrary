﻿using System.Linq;
using System.Windows.Forms;
using Zen.Barcode;
using System;
//-----------------------------------------------------------------------
// <copyright file="DatabaseController.cs">
//     Apache header
//
// Copyright 2017 Etienne Sainton
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// <author>Etienne</author>
// <date>1/11/2017</date>
//-----------------------------------------------------------------------

namespace BoardGamesLibrary.Controller
{
    using System.IO;
    using System.Diagnostics;
    using BoardGamesLibrary.Model;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Data;
    using static BoardGamesLibrary.Model.GamesDatabase;
    using System.Collections.Generic;

    /// <summary>
    /// This is the controller wich manages the data inside the database
    /// </summary>
    public class DatabaseController
    {
        /// <summary>
        /// This is the DataSet containing everything.
        /// </summary>
        private GamesDatabase gamesDatabase = new GamesDatabase();

        private BoardGameLibraryController boardGameLibraryController;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseController"/> class.
        /// </summary>
        public DatabaseController(BoardGameLibraryController boardGameLibraryController)
        {
            this.boardGameLibraryController = boardGameLibraryController;
            this.Load();
        }

        /// <summary>
        /// Gets the games database.
        /// </summary>
        /// <value>
        /// The games database.
        /// </value>
        public GamesDatabase GamesDatabase { get => gamesDatabase; }

        /// <summary>
        /// Adds the game.
        /// </summary>
        /// <param name="gameName">Name of the game.</param>
        /// <param name="minAge">The minimum age.</param>
        /// <param name="maxAge">The maximum age.</param>
        /// <param name="gameDescription">The game description.</param>
        public void AddGame(string gameName, int minAge, int maxAge, string gameDescription)
        {
            // Create a new row.
            GamesDatabase.BoardGamesRow newBoardGameRow;
            newBoardGameRow = gamesDatabase.BoardGames.NewBoardGamesRow();
            newBoardGameRow.name = gameName;
            newBoardGameRow.minAge = minAge;
            newBoardGameRow.maxAge = maxAge;
            newBoardGameRow.description = gameDescription;
            newBoardGameRow.BarCode = newBoardGameRow.boardGameId.ToString("000000000000");

            // Add the row to the Region table
            gamesDatabase.BoardGames.Rows.Add(newBoardGameRow);

            // Save the database
            this.Save();

            // Generate a barcode for this game:
            CodeEan13BarcodeDraw barcodeDraw = BarcodeDrawFactory.CodeEan13WithChecksum;
            Image barcodeImage = barcodeDraw.Draw(newBoardGameRow.BarCode, 60, 1);
            if(!Directory.Exists("barcodes"))
            {
                Directory.CreateDirectory("barcodes");
            }
            string codebarAddress = "barcodes/" + newBoardGameRow.name + "_" + newBoardGameRow.boardGameId + ".jpg";
            barcodeImage.Save(codebarAddress);

            this.boardGameLibraryController.LogManager.AddInformation("Just added " + newBoardGameRow.name + " to the collection. Id is : " + newBoardGameRow.boardGameId + ", codebar is " + codebarAddress);
        }

        /// <summary>
        /// Releases the game.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void ReleaseGame(int id)
        {
            GamesBorrowedRow[] gamesResults = (GamesBorrowedRow[])this.gamesDatabase.GamesBorrowed.Select("game = '" + id + "'");
            if (gamesResults.Length > 0)
            {
                List<int> boarGamesToReleaseIndex = new List<int>();
                foreach (GamesBorrowedRow tmpGame in gamesResults)
                {
                    this.gamesDatabase.GamesBorrowed.RemoveGamesBorrowedRow(tmpGame);
                }
            }
            this.Save();
            this.boardGameLibraryController.OnDatabaseUpdated(new EventArgs());
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            this.gamesDatabase.WriteXml("boardsGamesData.xml");
        }

        /// <summary>
        /// Looks for bar code.
        /// </summary>
        /// <param name="barCode">The bar code.</param>
        public BoardGamesRow LookForBarCode(int barCode)
        {
            BoardGamesRow[] results = (BoardGamesRow[]) this.gamesDatabase.BoardGames.Select("BarCode Like '" + barCode.ToString("000000000000") + "'");

            if (results.Length > 0)
                return results[0];
            else
                return null;
        }

        /// <summary>
        /// Looks for the name of the game
        /// </summary>
        /// <param name="gameName">The game name.</param>
        public BoardGamesRow LookForGameName(string gameName)
        {
            BoardGamesRow[] results = (BoardGamesRow[]) this.gamesDatabase.BoardGames.Select("name Like '" + gameName + "'");

            if (results.Length > 0)
                return results[0];
            else
                return null;
        }

        /// <summary>
        /// Loads this instance.
        /// </summary>
        public void Load()
        {
            if (File.Exists("boardsGamesData.xml"))
                this.gamesDatabase.ReadXml("boardsGamesData.xml");
            else
                this.boardGameLibraryController.LogManager.AddInformation("No database file has been found, this is normal for a first use only.");
        }

        /// <summary>
        /// Borrows the game.
        /// </summary>
        public void BorrowGame(int boardGameId, string userName)
        {
            // Get the game:
            BoardGamesRow[] gamesResults = (BoardGamesRow[])this.gamesDatabase.BoardGames.Select("boardGameId = '" + boardGameId + "'");
            BoardGamesRow gameBorrowed = null;
            if (gamesResults.Length > 0)
                gameBorrowed = gamesResults[0];

            // Get the user:
            UserRow[] userResults = (UserRow[])this.gamesDatabase.User.Select("name LIKE '" + userName + "'");
            UserRow userBorrowing = null;
            if (userResults.Length > 0)
                userBorrowing = userResults[0];
            else
            {
                GamesDatabase.UserRow newUserRow;
                newUserRow = gamesDatabase.User.NewUserRow();
                newUserRow.name = userName;
                newUserRow.category = 1;
                this.gamesDatabase.User.Rows.Add(newUserRow);
                this.Save();
                userBorrowing = newUserRow;
            }

            // Create a new row.
            if (gameBorrowed != null && userBorrowing != null)
            {
                GamesDatabase.GamesBorrowedRow newGameBorrowedRow;
                newGameBorrowedRow = gamesDatabase.GamesBorrowed.NewGamesBorrowedRow();
                newGameBorrowedRow.BoardGamesRow = gameBorrowed;
                newGameBorrowedRow.UserRow = userBorrowing;
                newGameBorrowedRow.startDate = DateTime.Today.ToString("yyyy/MM/dd");

                // Add the row to the Region table
                gamesDatabase.GamesBorrowed.Rows.Add(newGameBorrowedRow);

                // Save the database
                this.Save();

                this.boardGameLibraryController.LogManager.AddInformation(userBorrowing.name + " just borrowed " + gameBorrowed.name);
                this.boardGameLibraryController.OnDatabaseUpdated(new EventArgs());
            }
            else
            {
                this.boardGameLibraryController.LogManager.AddInformation("Error", ErrorLevel.ERROR);
            }
        }
    }
}
