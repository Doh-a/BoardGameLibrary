﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Drawing;
using System.Data;

namespace BoardGamesLibrary.Controller
{
    class PdfExporter
    {
        /// <summary>
        /// Generates the PDF.
        /// </summary>
        /// <param name="newDBController">The new database controller.</param>
        public void GeneratePDF(DatabaseController newDBController)
        {
            FileStream fs = new FileStream("Liste_de_jeux_"+DateTime.Today.ToString("yyyy_MM_dd") + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None);
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();

            Paragraph titleParagraph = new Paragraph("Liste de jeux\n");
            titleParagraph.Alignment = Element.ALIGN_CENTER;
            titleParagraph.Font.Size = 35;
            doc.Add(titleParagraph);
            Paragraph dateParagraph = new Paragraph(newDBController.GamesDatabase.BoardGames.Count + " jeux disponibles le " + DateTime.Today.ToString("dd/MM/yyyy") + "\n");
            dateParagraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(dateParagraph);

            float[] cellsWidth = new float[] { 2, 1, 1, 6 };
            PdfPTable table = new PdfPTable(cellsWidth);
            table.SpacingAfter = 20;
            table.SpacingBefore = 20;

            PdfPCell nameHeaderCell = new PdfPCell(new Paragraph("Nom du jeu"));
            PdfPCell minAgeHeaderCell = new PdfPCell(new Paragraph("Age minimum"));
            PdfPCell maxAgeHeaderCell = new PdfPCell(new Paragraph("Age maximum"));
            PdfPCell descriptionHeaderCell = new PdfPCell(new Paragraph("Description"));

            nameHeaderCell.BackgroundColor = BaseColor.YELLOW;
            minAgeHeaderCell.BackgroundColor = BaseColor.YELLOW;
            maxAgeHeaderCell.BackgroundColor = BaseColor.YELLOW;
            descriptionHeaderCell.BackgroundColor = BaseColor.YELLOW;

            table.AddCell(nameHeaderCell);
            table.AddCell(minAgeHeaderCell);
            table.AddCell(maxAgeHeaderCell);
            table.AddCell(descriptionHeaderCell);

            newDBController.GamesDatabase.BoardGames.DefaultView.Sort = "name ASC";
            DataRow[] gamesList = newDBController.GamesDatabase.BoardGames.Select("name NOT LIKE ''", "name ASC");
            for (int i = 0; i < gamesList.Length; i++)
            {
                PdfPCell nameCell = new PdfPCell(new Paragraph(((Model.GamesDatabase.BoardGamesRow) gamesList[i]).name));
                PdfPCell minAgeCell = new PdfPCell(new Paragraph(((Model.GamesDatabase.BoardGamesRow)gamesList[i]).minAge.ToString()));
                PdfPCell maxAgeCell = new PdfPCell(new Paragraph(((Model.GamesDatabase.BoardGamesRow)gamesList[i]).maxAge.ToString()));
                PdfPCell descriptionCell = new PdfPCell(new Paragraph(((Model.GamesDatabase.BoardGamesRow)gamesList[i]).description));

                if (i % 2 == 0)
                {
                    nameCell.BackgroundColor = BaseColor.LIGHT_GRAY;
                    minAgeCell.BackgroundColor = BaseColor.LIGHT_GRAY;
                    maxAgeCell.BackgroundColor = BaseColor.LIGHT_GRAY;
                    descriptionCell.BackgroundColor = BaseColor.LIGHT_GRAY;
                }

                table.AddCell(nameCell);
                table.AddCell(minAgeCell);
                table.AddCell(maxAgeCell);
                table.AddCell(descriptionCell);
            }
            doc.Add(table);

            doc.Close();
        }
    }
}
