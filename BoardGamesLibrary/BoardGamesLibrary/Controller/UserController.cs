﻿using static BoardGamesLibrary.Model.GamesDatabase;

namespace BoardGamesLibrary.Controller
{
    /// <summary>
    /// 
    /// </summary>
    public class UserController
    {
        /// <summary>
        /// The user data
        /// </summary>
        private UserRow userData;

        /// <summary>
        /// The database controller
        /// </summary>
        private DatabaseController databaseController;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserController"/> class.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="newDatabaseController">The new database controller.</param>
        public UserController(int userId, DatabaseController newDatabaseController)
        {
            this.databaseController = newDatabaseController;

            UserRow[] tmpResult = (UserRow[])this.databaseController.GamesDatabase.User.Select("userId = " + userId);

            if(tmpResult.Length > 0)
            {
                this.userData = tmpResult[0];
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get => this.userData.name; }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get => this.userData.userId; }
    }
}