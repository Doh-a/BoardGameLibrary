﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BoardGamesLibrary.Controller
{
    /// <summary>
    /// Main controller for this app
    /// </summary>
    public class BoardGameLibraryController
    {
        public event EventHandler DatabaseUpdated;

        /// <summary>
        /// The database controller
        /// </summary>
        private DatabaseController databaseController;

        /// <summary>
        /// My configuration
        /// </summary>
        private BoardGameLibraryConfig myConfig;

        /// <summary>
        /// The configuration folder
        /// </summary>
        private string configFolder = "Configs";

        /// <summary>
        /// The configuration file
        /// </summary>
        private string configFile = "boardGameLibrary.conf";

        /// <summary>
        /// The log manager
        /// </summary>
        private LogManager logManager = new LogManager();

        /// <summary>
        /// The bar code reader
        /// </summary>
        private BarCodeReader barCodeReader;

        /// <summary>
        /// Gets the database.
        /// </summary>
        /// <value>
        /// The database.
        /// </value>
        public DatabaseController Database { get => databaseController; }

        /// <summary>
        /// Gets or sets the bar code reader.
        /// </summary>
        /// <value>
        /// The bar code reader.
        /// </value>
        public BarCodeReader BarCodeReader { get => barCodeReader; set => barCodeReader = value; }

        /// <summary>
        /// Gets or sets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public BoardGameLibraryConfig Config { get => myConfig; set => myConfig = value; }

        /// <summary>
        /// Gets the log manager.
        /// </summary>
        /// <value>
        /// The log manager.
        /// </value>
        public LogManager LogManager { get => logManager;}

        /// <summary>
        /// Occurs when [bar code detected].
        /// </summary>
        public event EventHandler BarCodeDetected;

        /// <summary>
        /// Initializes a new instance of the <see cref="BoardGameLibraryController"/> class.
        /// </summary>
        public BoardGameLibraryController()
        {
            // Import the settings (or create the initial file)
            this.myConfig = new BoardGameLibraryConfig();

            // Create the config folder once
            if(!Directory.Exists(this.configFolder))
            {
                Directory.CreateDirectory(this.configFolder);
            }

            // Create the config file once
            if (File.Exists(this.configFolder + "/" + this.configFile))
            {
                // Load the configuration of this computer:
                FileStream readFileStream = new FileStream(this.configFolder + "/" + this.configFile, FileMode.Open, FileAccess.Read, FileShare.Read);
                XmlSerializer serializer = new XmlSerializer(typeof(BoardGameLibraryConfig));
                this.myConfig = (BoardGameLibraryConfig)serializer.Deserialize(readFileStream);
                readFileStream.Close();
            }
            else
            {
                XmlSerializer serializer = new XmlSerializer(typeof(BoardGameLibraryConfig));
                TextWriter writeFileStream = new StreamWriter(this.configFolder + "/" + this.configFile);
                serializer.Serialize(writeFileStream, this.myConfig);
                writeFileStream.Close();
            }

            // Load the database
            databaseController = new DatabaseController(this);

            // Start the code bar reader if neede
            if (this.myConfig.UseBarCodeReader)
            {
                barCodeReader = new BarCodeReader(this.logManager, this.myConfig.BarCodeComPort);
                barCodeReader.BarCodeDetected += BarCodeReader_BarCodeDetected; ;
            }
        }

        /// <summary>
        /// Handles the BarCodeDetected event of the BarCodeReader control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void BarCodeReader_BarCodeDetected(object sender, EventArgs e)
        {
            EventHandler handler = BarCodeDetected;
            if (handler != null)
            {
                handler(this, e);
            }            
        }

        /// <summary>
        /// Raises the <see cref="E:DatabaseUpdated" /> event.
        /// </summary>
        /// <param name="newEventArg">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnDatabaseUpdated(EventArgs newEventArg)
        {
            EventHandler handler = this.DatabaseUpdated;
            if (handler != null)
            {
                handler(this, newEventArg);
            }
        }
    }
}
