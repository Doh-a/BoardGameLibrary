﻿using BoardGamesLibrary.Model.EvengArgs;
//-----------------------------------------------------------------------
// <copyright file="BarCodeReader.cs">
//     Apache header
//
// Copyright 2017 Etienne Sainton
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// <author>Etienne</author>
// <date>1/11/2017</date>
//-----------------------------------------------------------------------

using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;

namespace BoardGamesLibrary.Controller
{
    /// <summary>
    /// Manages the reader.
    /// </summary>
    public class BarCodeReader
    {
        /// <summary>
        /// The serial port
        /// </summary>
        private SerialPort serialPort;

        /// <summary>
        /// The should continue
        /// </summary>
        private bool shouldContinue;

        /// <summary>
        /// Occurs when [bar code detected].
        /// </summary>
        public event EventHandler BarCodeDetected;

        public BarCodeReader(LogManager logManager, string comPort)
        { 
            StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
            Thread readThread = new Thread(Read);

            // Create a new SerialPort object with default settings.
            serialPort = new SerialPort();

            // Allow the user to set the appropriate properties.
            serialPort.PortName = comPort;
            serialPort.BaudRate = 9600;
            serialPort.Parity = Parity.None;
            serialPort.DataBits = 8;
            serialPort.StopBits = StopBits.One;
            serialPort.Handshake = Handshake.None;

            // Set the read/write timeouts
            serialPort.ReadTimeout = 500;
            serialPort.WriteTimeout = 500;

            try {
                serialPort.Open();
                shouldContinue = true;
            }
            catch(IOException ioEx)
            {
                logManager.AddInformation("Error opening com port : " + ioEx.Message + "\n\t" + ioEx.StackTrace, ErrorLevel.ERROR);
                MessageBox.Show("Error opening com port : " + ioEx.Message, "Error in your config file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }

            readThread.Start();
        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        public void Read()
        {
            while (shouldContinue)
            {
                try
                {
                    string message = serialPort.ReadLine();
                    Console.WriteLine(message);

                    // The message usually looks like \u0002 REAL ID \r, so we will remove them.
                    // sometime in front of this they contains \u0003 that we want to remove as well.
                    if (message.Length == 15)
                        message = message.Substring(2, message.Length - 2);
                    else
                        message = message.Substring(1, message.Length - 1);
                    message = message.Substring(0, message.Length - 1);
                    
                    BarCodeDetectedEventArgs tmpBarCodeDetectedEventArgs = new BarCodeDetectedEventArgs();
                    tmpBarCodeDetectedEventArgs.barCode = message;
                    OnBarCodeDetected(tmpBarCodeDetectedEventArgs);
                }
                catch (TimeoutException) {; }
            }
        }

        private void OnBarCodeDetected(EventArgs e)
        {
            EventHandler handler = BarCodeDetected;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public void Stop()
        {
            this.shouldContinue = false;
        }
    }
}