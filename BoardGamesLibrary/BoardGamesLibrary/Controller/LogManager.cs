﻿//-----------------------------------------------------------------------
// <copyright file="LogManager.cs">
//     Apache header
//
// Copyright 2017 Etienne Sainton
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// <author>Etienne</author>
// <date>1/11/2017</date>
//-----------------------------------------------------------------------

namespace BoardGamesLibrary.Controller
{
    using BoardGamesLibrary.Models.EventArgsDesc;
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Forms;

    public enum ErrorLevel
    {
        INFORMATION,
        WARNING,
        ERROR,
        PRIVATEWARNING,
        DISPLAYEDINFORMATION
    }

    /// <summary>
    /// This class formats the logs, save them in a file, and fires events if other classes want to use them.
    /// </summary>
    public class LogManager
    {
        /// <summary>
        /// The last message
        /// </summary>
        private string lastMessage = string.Empty;

        /// <summary>
        /// The level
        /// </summary>
        private ErrorLevel level = 0;

        /// <summary>
        /// The log file
        /// </summary>
        private string logFile = "BoardGamesLibrary.txt";

        /// <summary>
        /// The path
        /// </summary>
        private string path;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogManager"/> class.
        /// </summary>
        public LogManager()
        {
            string logDirectory = Path.GetDirectoryName(Application.ExecutablePath) + "\\Logs\\";
            if (!Directory.Exists(logDirectory))
            {
                Directory.CreateDirectory(logDirectory);
            }

            this.path = logDirectory + this.logFile;

            if (File.Exists(this.path)) 
            {
                File.Delete(this.path);
            }

            // Create a file to write to.
            using (StreamWriter sw = File.CreateText(this.path))
            {
                sw.WriteLine("Log is created : " + DateTime.Now.ToString("yyyy-MM-dd-HH") + "\n\r");
            }
        }

        /// <summary>
        /// Occurs when [information updated].
        /// </summary>
        public event EventHandler InformationUpdated;

        /// <summary>
        /// Adds the information as an low level issue.
        /// </summary>
        /// <param name="newText">The new text.</param>
        public void AddInformation(string newText)
        {
            this.AddInformation(newText, 0);
        }

        /// <summary>
        /// Add information
        /// </summary>
        /// <param name="newText">The text to display / save.</param>
        /// <param name="newLevel">The level of this information (0 : information, 1 : warning, 2 : error)</param>
        /// <remarks>This is the only place where we save everything in log, and where we use Debug.</remarks>
        public void AddInformation(string newText, ErrorLevel newLevel)
        {
            // Change the current informations
            this.lastMessage = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm") + ":" + DateTime.Now.ToString("ss") + " - " + newText;
            this.level = newLevel;

            // Define the text for this level :
            string levelString = "Undefined level : ";
            switch (this.level)
            {
                case ErrorLevel.INFORMATION:
                    levelString = "Information : ";
                    break;

                case ErrorLevel.WARNING:
                    levelString = "Warning : ";
                    break;

                case ErrorLevel.ERROR:
                    levelString = "Error : ";
                    break;

                case ErrorLevel.PRIVATEWARNING:
                    levelString = "Private warning : ";
                    break;

                case ErrorLevel.DISPLAYEDINFORMATION:
                    levelString = "Displayed information: ";
                    break;
            }

            // Save it if needed :
            if (this.logFile != null)
            {
                try
                {
                    // This text is already added, append the text
                    using (StreamWriter sw = File.AppendText(this.path))
                    {
                        sw.WriteLine(levelString + this.lastMessage);
                        sw.Close();
                    }
                }
                catch (Exception exception)
                {
                    Debug.WriteLine("Can't write log in the log file : " + exception);
                    Console.WriteLine("Can't write log in the log file : " + exception);
                }
            }

            Debug.WriteLine(levelString + this.lastMessage);
            Console.WriteLine(levelString + this.lastMessage);

            if(this.level == ErrorLevel.DISPLAYEDINFORMATION)
            {
                MessageBox.Show(this.lastMessage);
            }

            LogManagerInformationEventsArgs tmpInformationEventArgs = new LogManagerInformationEventsArgs();
            tmpInformationEventArgs.LastMessage = this.lastMessage;
            this.NotifyObservers(tmpInformationEventArgs);
        }

        #region "Observers"

        /// <summary>
        /// There are some changes, observers have to know it
        /// </summary>
        /// <param name="e">The event passed, should be an <typeparamref name="LogManagerInformationEventsArgs"/></param>
        public void NotifyObservers(EventArgs e)
        {
            EventHandler handler = this.InformationUpdated;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion "Observers"
    }
}
