﻿using System.Runtime.CompilerServices;
using System.Collections.Specialized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BoardGamesLibrary.Model.GamesDatabase;

namespace BoardGamesLibrary.Controller
{
    public class GameHistory
    {
        /// <summary>
        /// The user name
        /// </summary>
        private string userName;

        /// <summary>
        /// The start date
        /// </summary>
        private string startDate;

        /// <summary>
        /// The end date
        /// </summary>
        private string endDate;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameHistory"/> class.
        /// </summary>
        /// <param name="newUserName">New name of the user.</param>
        /// <param name="newStartDate">The new start date.</param>
        /// <param name="newEndDate">The new end date.</param>
        public GameHistory(string newUserName, string newStartDate, string newEndDate)
        {
            this.userName = newUserName;
            this.startDate = newStartDate;
            this.endDate = newEndDate;
        }

        /// <summary>
        /// Gets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public string EndDate { get => endDate; }

        /// <summary>
        /// Gets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public string StartDate { get => startDate; }

        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get => userName; }
    }

    /// <summary>
    /// Access to the game data
    /// </summary>
    public class GameController
    {
        /// <summary>
        /// The data
        /// </summary>
        private BoardGamesRow data;

        /// <summary>
        /// The games borrowed
        /// </summary>
        private GamesBorrowedRow[] gamesBorrowed;

        /// <summary>
        /// The database controller
        /// </summary>
        private DatabaseController databaseController;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameController"/> class.
        /// </summary>
        /// <param name="gameId">The game identifier.</param>
        public GameController(int gameId, DatabaseController newDatabaseController)
        {
            this.databaseController = newDatabaseController;

            BoardGamesRow[] tmpResult = (BoardGamesRow[]) this.databaseController.GamesDatabase.BoardGames.Select("boardGameId = " + gameId);
            if(tmpResult.Length > 0)
            {
                this.data = tmpResult[0];

                this.gamesBorrowed = (GamesBorrowedRow[])this.databaseController.GamesDatabase.GamesBorrowed.Select("game = " + gameId);
            }
        }

        /// <summary>
        /// Games the identifier.
        /// </summary>
        /// <returns></returns>
        public int Id { get => this.data.boardGameId; }

        public string Name { get => this.data.name; }

        public int MinAge { get => this.data.minAge; }

        public int MaxAge { get => this.data.maxAge; }

        public string Description { get => this.data.description; }

        public List<GameHistory> History
        {
            get {
                List<GameHistory> newHistoryList = new List<GameHistory>();

                foreach(GamesBorrowedRow tmpBorrowedRow in this.gamesBorrowed)
                {
                    UserController tmpUser = new UserController(tmpBorrowedRow.user, this.databaseController);
                    string tmpStartDate = "-1";
                    string tmpEndDate = "-1";

                    if (tmpBorrowedRow["startDate"] != DBNull.Value)
                        tmpStartDate = tmpBorrowedRow.startDate;
                    if (tmpBorrowedRow["endDate"] != DBNull.Value)
                        tmpEndDate = tmpBorrowedRow.endDate;

                    GameHistory tmpHistory = new GameHistory(tmpUser.Name, tmpStartDate, tmpEndDate);
                }

                return newHistoryList;
            }
        }
    }
}
