﻿//-----------------------------------------------------------------------
// <copyright file="LogManagerInformationEventsArgs.cs" company="Alterface">
//     Copyright (c) Alterface. All rights reserved.
// </copyright>
// <author>Etienne</author>
// <date>24/7/2017</date>
//-----------------------------------------------------------------------

namespace Dispatcher.Models.EventArgsDesc
{
    using System;

    /// <summary>
    /// Event Args used for log manager information updated.
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    public class LogManagerInformationEventsArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the last message.
        /// </summary>
        /// <value>
        /// The last message.
        /// </value>
        public string LastMessage { get; set; }
    }
}
