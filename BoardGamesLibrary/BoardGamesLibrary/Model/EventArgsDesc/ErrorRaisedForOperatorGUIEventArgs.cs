﻿//-----------------------------------------------------------------------
// <copyright file="ErrorRaisedForOperatorGUIEventArgs.cs" company="Alterface">
//     Copyright (c) Alterface. All rights reserved.
// </copyright>
// <author>Etienne</author>
// <date>25/7/2017</date>
//-----------------------------------------------------------------------

namespace Dispatcher.Models.EventArgsDesc
{
    using System;

    /// <summary>
    /// Something went wrong somewhere and we would like to display it on the operator GUI
    /// </summary>
    public class ErrorRaisedForOperatorGUIEventArgs : EventArgs
    {
        /// <summary>
        /// The error message
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }

            set
            {
                this.errorMessage = value;
            }
        }
    }
}
