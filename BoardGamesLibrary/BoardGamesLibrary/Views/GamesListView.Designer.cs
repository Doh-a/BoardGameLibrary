﻿namespace BoardGamesLibrary.Views
{
    partial class GamesListView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GamesListView));
            this.boardGamesDataGridView = new System.Windows.Forms.DataGridView();
            this.NameGame = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MinAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaxAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonPDFExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.boardGamesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // boardGamesDataGridView
            // 
            this.boardGamesDataGridView.AllowUserToAddRows = false;
            this.boardGamesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.boardGamesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameGame,
            this.MinAge,
            this.MaxAge,
            this.Description});
            this.boardGamesDataGridView.Location = new System.Drawing.Point(13, 13);
            this.boardGamesDataGridView.Name = "boardGamesDataGridView";
            this.boardGamesDataGridView.Size = new System.Drawing.Size(860, 495);
            this.boardGamesDataGridView.TabIndex = 0;
            // 
            // NameGame
            // 
            this.NameGame.HeaderText = "Nom";
            this.NameGame.Name = "NameGame";
            // 
            // MinAge
            // 
            this.MinAge.HeaderText = "Age minimum";
            this.MinAge.Name = "MinAge";
            // 
            // MaxAge
            // 
            this.MaxAge.HeaderText = "Age maximum";
            this.MaxAge.Name = "MaxAge";
            // 
            // Description
            // 
            this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            // 
            // buttonPDFExport
            // 
            this.buttonPDFExport.Location = new System.Drawing.Point(13, 528);
            this.buttonPDFExport.Name = "buttonPDFExport";
            this.buttonPDFExport.Size = new System.Drawing.Size(75, 23);
            this.buttonPDFExport.TabIndex = 1;
            this.buttonPDFExport.Text = "Export PDF";
            this.buttonPDFExport.UseVisualStyleBackColor = true;
            this.buttonPDFExport.Click += new System.EventHandler(this.buttonPDFExport_Click);
            // 
            // GamesListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 563);
            this.Controls.Add(this.buttonPDFExport);
            this.Controls.Add(this.boardGamesDataGridView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GamesListView";
            this.Text = "Liste des jeux";
            ((System.ComponentModel.ISupportInitialize)(this.boardGamesDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView boardGamesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn GameName;
        private System.Windows.Forms.Button buttonPDFExport;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameGame;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaxAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
    }
}