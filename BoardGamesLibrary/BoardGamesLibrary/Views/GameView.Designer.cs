﻿namespace BoardGamesLibrary.Views
{
    partial class GameView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameView));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.descriptionText = new System.Windows.Forms.RichTextBox();
            this.maxAgeText = new System.Windows.Forms.Label();
            this.minAgeText = new System.Windows.Forms.Label();
            this.nameText = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.giveBackButton = new System.Windows.Forms.Button();
            this.borrowButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.historyPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.borrowerNameTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.descriptionText);
            this.groupBox1.Controls.Add(this.maxAgeText);
            this.groupBox1.Controls.Add(this.minAgeText);
            this.groupBox1.Controls.Add(this.nameText);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(494, 174);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Jeu";
            // 
            // descriptionText
            // 
            this.descriptionText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.descriptionText.Location = new System.Drawing.Point(109, 86);
            this.descriptionText.Name = "descriptionText";
            this.descriptionText.ReadOnly = true;
            this.descriptionText.Size = new System.Drawing.Size(379, 82);
            this.descriptionText.TabIndex = 7;
            this.descriptionText.Text = "";
            // 
            // maxAgeText
            // 
            this.maxAgeText.AutoSize = true;
            this.maxAgeText.Location = new System.Drawing.Point(106, 64);
            this.maxAgeText.Name = "maxAgeText";
            this.maxAgeText.Size = new System.Drawing.Size(66, 13);
            this.maxAgeText.TabIndex = 6;
            this.maxAgeText.Text = "maxAgeText";
            // 
            // minAgeText
            // 
            this.minAgeText.AutoSize = true;
            this.minAgeText.Location = new System.Drawing.Point(106, 41);
            this.minAgeText.Name = "minAgeText";
            this.minAgeText.Size = new System.Drawing.Size(63, 13);
            this.minAgeText.TabIndex = 5;
            this.minAgeText.Text = "minAgeText";
            // 
            // nameText
            // 
            this.nameText.AutoSize = true;
            this.nameText.Location = new System.Drawing.Point(106, 20);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(54, 13);
            this.nameText.TabIndex = 4;
            this.nameText.Text = "nameText";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Description:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Age maximum:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Age minmum:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.borrowerNameTextBox);
            this.groupBox2.Controls.Add(this.giveBackButton);
            this.groupBox2.Controls.Add(this.borrowButton);
            this.groupBox2.Location = new System.Drawing.Point(13, 193);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(494, 96);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Actions";
            // 
            // giveBackButton
            // 
            this.giveBackButton.Location = new System.Drawing.Point(10, 19);
            this.giveBackButton.Name = "giveBackButton";
            this.giveBackButton.Size = new System.Drawing.Size(97, 23);
            this.giveBackButton.TabIndex = 1;
            this.giveBackButton.Text = "Rendre";
            this.giveBackButton.UseVisualStyleBackColor = true;
            this.giveBackButton.Click += new System.EventHandler(this.giveBackButton_Click);
            // 
            // borrowButton
            // 
            this.borrowButton.Location = new System.Drawing.Point(10, 57);
            this.borrowButton.Name = "borrowButton";
            this.borrowButton.Size = new System.Drawing.Size(97, 23);
            this.borrowButton.TabIndex = 0;
            this.borrowButton.Text = "Emprunter";
            this.borrowButton.UseVisualStyleBackColor = true;
            this.borrowButton.Click += new System.EventHandler(this.borrowButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.historyPanel);
            this.groupBox3.Location = new System.Drawing.Point(13, 295);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(494, 268);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Emprunts";
            // 
            // historyPanel
            // 
            this.historyPanel.Location = new System.Drawing.Point(0, 20);
            this.historyPanel.Name = "historyPanel";
            this.historyPanel.Size = new System.Drawing.Size(494, 281);
            this.historyPanel.TabIndex = 0;
            // 
            // borrowerNameTextBox
            // 
            this.borrowerNameTextBox.Location = new System.Drawing.Point(113, 59);
            this.borrowerNameTextBox.Name = "borrowerNameTextBox";
            this.borrowerNameTextBox.Size = new System.Drawing.Size(375, 20);
            this.borrowerNameTextBox.TabIndex = 2;
            // 
            // GameView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 575);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GameView";
            this.Text = "GameView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox descriptionText;
        private System.Windows.Forms.Label maxAgeText;
        private System.Windows.Forms.Label minAgeText;
        private System.Windows.Forms.Label nameText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button giveBackButton;
        private System.Windows.Forms.Button borrowButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox borrowerNameTextBox;
        private System.Windows.Forms.FlowLayoutPanel historyPanel;
    }
}