﻿using BoardGamesLibrary.Model.EvengArgs;
//-----------------------------------------------------------------------
// <copyright file="MainForm.cs">
//     Apache header
//
// Copyright 2017 Etienne Sainton
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// <author>Etienne</author>
// <date>1/11/2017</date>
//-----------------------------------------------------------------------

/// <summary>
/// 
/// </summary>
namespace BoardGamesLibrary.Views
{
    using BoardGamesLibrary.Controller;
    using BoardGamesLibrary.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;
    using static BoardGamesLibrary.Model.GamesDatabase;

    /// <summary>
    /// The main view displayed.
    /// It manages end of the application and logs as well.
    /// This is the entry point of the app.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class MainForm : Form
    {
        /// <summary>
        /// The main controller
        /// </summary>
        private BoardGameLibraryController mainController;

        private Dictionary<int, GamesBorrowedRow> borrowedGamesViewMap = new Dictionary<int, GamesBorrowedRow>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        public MainForm()
        {
            Application.ApplicationExit += new EventHandler(this.OnApplicationExit);

            mainController = new BoardGameLibraryController();
            mainController.DatabaseUpdated += MainController_DatabaseUpdated;

            InitializeComponent();

            DisplayGamesBorrowed();
        }

        /// <summary>
        /// Handles the DatabaseUpdated event of the MainController control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void MainController_DatabaseUpdated(object sender, EventArgs e)
        {
            this.DisplayGamesBorrowed();
        }

        /// <summary>
        /// Displays the games borrowed.
        /// </summary>
        private void DisplayGamesBorrowed()
        {
            if (this.borrowedGamesGridView.InvokeRequired)
            {
                this.borrowedGamesGridView.BeginInvoke(new MethodInvoker(delegate { this.DisplayGamesBorrowed(); }));
            }
            else
            {
                this.borrowedGamesGridView.Rows.Clear();
                borrowedGamesViewMap = new Dictionary<int, GamesBorrowedRow>();
                foreach (GamesDatabase.GamesBorrowedRow tmpBoardGame in this.mainController.Database.GamesDatabase.GamesBorrowed.Rows)
                {
                    int rowId = this.borrowedGamesGridView.Rows.Add();

                    // Grab the new row!
                    DataGridViewRow row = this.borrowedGamesGridView.Rows[rowId];

                    // Add the data
                    row.Cells["Game"].Value = tmpBoardGame.BoardGamesRow.name;
                    row.Cells["User"].Value = tmpBoardGame.UserRow.name;

                    if (tmpBoardGame["startDate"] != DBNull.Value)
                        row.Cells["Since"].Value = tmpBoardGame.startDate;

                    borrowedGamesViewMap.Add(rowId, tmpBoardGame);

                }
                //this.borrowedGamesGridView.Sort(this.borrowedGamesGridView.Columns[2], ListSortDirection.Ascending);
            }
        }

        /// <summary>
        /// Handles the BarCodeDetected event of the BarCodeReader control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BarCodeReader_BarCodeDetected(object sender, EventArgs e)
        {
            this.searchGame(((BarCodeDetectedEventArgs)e).barCode);
        }

        /// <summary>
        /// Called when [application exit].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnApplicationExit(object sender, EventArgs e)
        {
            this.mainController.Database.Save();
            if(this.mainController.BarCodeReader != null)
                this.mainController.BarCodeReader.Stop();
        }

        /// <summary>
        /// Handles the Click event of the addGameToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void addGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddGameView newAddGameViewWindow = new AddGameView(this.mainController.Database);
            newAddGameViewWindow.Show();
        }

        /// <summary>
        /// Handles the Click event of the listeDesJeuxToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void listeDesJeuxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GamesListView gamesListViewWindow = new GamesListView(this.mainController.Database, this.mainController.LogManager);
            gamesListViewWindow.Show();
        }

        /// <summary>
        /// Handles the Click event of the searchButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void searchButton_Click(object sender, EventArgs e)
        {            
            if(this.searchTextBox.Text != "")
            {
                this.searchGame(this.searchTextBox.Text);               
            }
        }

        /// <summary>
        /// Searches the game.
        /// </summary>
        /// <param name="text">The text.</param>
        private void searchGame(string text)
        {
            int barCode = 0;
            BoardGamesRow gameData = null;
            if (int.TryParse(text, out barCode))
            {
                // Look for the bar code:
                gameData = this.mainController.Database.LookForBarCode(barCode);
            }
            else
            {
                // Look for the name:
                gameData = this.mainController.Database.LookForGameName(text);
            }

            if(gameData != null)
            {
                GameController tmpGame = new GameController(gameData.boardGameId, this.mainController.Database);
                GameView newGameView = new GameView(tmpGame, this.mainController.Database, this.mainController.LogManager);
                newGameView.Show();
            }
            else
            {
                this.mainController.LogManager.AddInformation("No game can be find with " + text, ErrorLevel.DISPLAYEDINFORMATION);
            }
        }

        private void borrowedGamesGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 0)
            {
                GameController tmpGame = new GameController(borrowedGamesViewMap[e.RowIndex].BoardGamesRow.boardGameId, this.mainController.Database);
                GameView newGameView = new GameView(tmpGame, this.mainController.Database, this.mainController.LogManager);
                newGameView.Show();
            }
        }
    }
}
