﻿//-----------------------------------------------------------------------
// <copyright file="AddGameView.cs">
//     Apache header
//
// Copyright 2017 Etienne Sainton
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// <author>Etienne</author>
// <date>1/11/2017</date>
//-----------------------------------------------------------------------

namespace BoardGamesLibrary.Views
{    
    using System;
    using System.Windows.Forms;
    using BoardGamesLibrary.Controller;

    /// <summary>
    /// This form is dedicated to add a new game in the database
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class AddGameView : Form
    {
        /// <summary>
        /// The database controller
        /// </summary>
        private DatabaseController databaseController;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddGameView"/> class.
        /// </summary>
        /// <param name="controller">The controller.</param>
        public AddGameView(DatabaseController controller)
        {
            this.databaseController = controller;

            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the addButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void AddButton_Click(object sender, EventArgs e)
        {
            this.databaseController.AddGame(this.nameTextBox.Text, (int)this.minAgeNumericUpDown.Value, (int)this.maxAgeNumericUpDown.Value, this.descriptionTextBox.Text);
            this.Close();
        }
    }
}