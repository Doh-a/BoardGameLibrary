﻿using BoardGamesLibrary.Controller;
using BoardGamesLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static BoardGamesLibrary.Model.GamesDatabase;

namespace BoardGamesLibrary.Views
{
    public partial class GameView : Form
    {
        private DatabaseController databaseController;
        private GameController game;
        private LogManager logManager;

        public GameView(GameController game, DatabaseController database, LogManager logManager)
        {
            InitializeComponent();

            this.databaseController = database;
            this.game = game;
            this.logManager = logManager;

            this.nameText.Text = game.Name;
            this.minAgeText.Text = game.MinAge.ToString();
            this.maxAgeText.Text = game.MaxAge.ToString();
            this.descriptionText.Text = game.Description;

            // Fill the history :
            foreach (GameHistory tmpHistory in game.History)
            {
                Label historyLabel = new Label();
                historyLabel.Text = tmpHistory.UserName + " du " + tmpHistory.StartDate + " au " + tmpHistory.EndDate;
                this.historyPanel.Controls.Add(historyLabel);
            }                
        }

        /// <summary>
        /// Handles the Click event of the borrowButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void borrowButton_Click(object sender, EventArgs e)
        {
            if (this.borrowerNameTextBox.Text != "")
            {
                this.databaseController.BorrowGame(this.game.Id, this.borrowerNameTextBox.Text);
                this.logManager.AddInformation("Le jeu a bien été emprunté par " + this.borrowerNameTextBox.Text, ErrorLevel.DISPLAYEDINFORMATION);
            }
            else
            {
                this.logManager.AddInformation("Veuillez entrer le nom de la personne qui emprunte le jeu.", ErrorLevel.DISPLAYEDINFORMATION);
            }

        }

        /// <summary>
        /// Handles the Click event of the giveBackButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void giveBackButton_Click(object sender, EventArgs e)
        {
            this.databaseController.ReleaseGame(this.game.Id);
        }

        /// <summary>
        /// Handles the Click event of the deleteButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void deleteButton_Click(object sender, EventArgs e)
        {

        }
    }
}
