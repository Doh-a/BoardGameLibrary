﻿//-----------------------------------------------------------------------
// <copyright file="GamesListView.cs">
//     Apache header
//
// Copyright 2017 Etienne Sainton
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// <author>Etienne</author>
// <date>1/11/2017</date>
//-----------------------------------------------------------------------

using System.Diagnostics;
using BoardGamesLibrary.Model;
using BoardGamesLibrary.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BoardGamesLibrary.Views
{
    public partial class GamesListView : Form
    {
        private DatabaseController gamesDatabaseController;
        private LogManager logManager;

        public GamesListView(DatabaseController databaseController, LogManager newLogManager)
        {
            this.gamesDatabaseController = databaseController;
            this.logManager = newLogManager;

            InitializeComponent();

            foreach (GamesDatabase.BoardGamesRow tmpBoardGame in this.gamesDatabaseController.GamesDatabase.BoardGames.Rows)
            {
                int rowId = this.boardGamesDataGridView.Rows.Add();

                // Grab the new row!
                DataGridViewRow row = this.boardGamesDataGridView.Rows[rowId];

                // Add the data
                row.Cells["NameGame"].Value = tmpBoardGame.name;
                row.Cells["MinAge"].Value = tmpBoardGame.minAge;
                row.Cells["MaxAge"].Value = tmpBoardGame.maxAge;
                row.Cells["Description"].Value = tmpBoardGame.description;
            }
            this.boardGamesDataGridView.Sort(this.boardGamesDataGridView.Columns[0], ListSortDirection.Ascending);
        }

        /// <summary>
        /// Handles the Click event of the buttonPDFExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonPDFExport_Click(object sender, EventArgs e)
        {
            PdfExporter pdfExporter = new PdfExporter();
            pdfExporter.GeneratePDF(this.gamesDatabaseController);
            this.logManager.AddInformation("PDF créé !", ErrorLevel.DISPLAYEDINFORMATION);
        }
    }
}
